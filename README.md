To run:

    after cloning run npm install
    after it finishes node app.js to start the app

Routes:

    / -> Loop with showing messages
    /stomislis -> Adding messages
    /stomislis/reset -> Removes all messages from db

Starting and stopping production:

		/home/domarku/webapps/stomislis/bin/stop -> Stopping app
		/home/domarku/webapps/stomislis/bin/start -> Starting app