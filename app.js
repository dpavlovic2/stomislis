var express = require('express')
bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    require(__dirname + '/models/message.js'),
    Message = mongoose.model('Message'),
    socketIO = require('socket.io'),
    async = require('async'),
    cron = require('cron').CronJob,
    app = express();

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

app.use('/static', express.static(__dirname + '/static'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/stomislis', (req, res) => {
    return res.render('index');
});

app.get('/list', (req, res) => {
    Message.find({}, (err, messages) => {
        if (err) {
            console.log(err);
        } else {
            return res.status(200).send(messages);
        }
    });
});

app.get('/', (req, res) => {
    return res.render('messages');
});

app.get('/stomislis/reset', (req, res) => {
    Message.remove({}, () => { });
    return res.redirect('/stomislis');
});

app.post('/addMessage', (req, res) => {
    var body = req.body;

    if (body.message) {
        new Message({ text: body.message })
            .save((err, saved) => {
                if (err) {
                    console.log(err);
                } else {
                    websockets.clients().emit('addedMessage', {
                        message: saved
                    });
                    res.redirect('/stomislis');
                }
            });
    }
});

mongoose.connect('mongodb://stomislis:stomislis@ds023654.mlab.com:23654/domarku')

var server = app.listen(32013, () => {
    console.log('Server running!');
});

var websockets = socketIO.listen(server);
var job = null;

websockets.on('connection', (socket) => {
    socket.on('disconnect', () => {
        if (job) {
            job.stop();
        }
    });


    async.waterfall([
        (cb) => {
            Message.findOne({}, (err, wanted) => {
                if (err) {
                    cb(err);
                } else {
                    cb(null, wanted);
                }
            });
        },
        (message, cb) => {
            if (message) {
                if (message.__v) {
                    delete message.__v;
                }

                socket.emit('newMessage', { message: message });

                cb(null);
            }
        },
        (cb) => {
            job = new cron('*/5 * * * * *', () => {
                socket.emit('currentMessage', null, (msg) => {
                    Message.findOne().where('_id').gt(msg._id).exec(function (err, nextMessage) {
                        if (err) {
                            cb(err);
                        }

                        if (nextMessage) {
                            socket.emit('newMessage', { message: nextMessage });
                        } else {
                            Message.findOne({}, (err, wanted) => {
                                if (err) {
                                    cb(err);
                                } else {
                                    socket.emit('newMessage', { message: wanted });
                                }
                            });
                        }
                    });
                });
            });

            job.start();
        }
    ], (err) => {
        return console.log(err);
    });
});