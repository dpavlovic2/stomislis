var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rimraf = require('gulp-rimraf');

var js = {
    dist: './static/dist/bundle.js',
    src: './static/js/*.js'
};

gulp.task('js:clear', (cb) => gulp.src(js.dist, { read: false }).pipe(rimraf()));

gulp.task('js:build', ['js:clear'], () => gulp.src(js.src).pipe(concat(js.dist))
        .pipe(uglify())
        .pipe(gulp.dest('.')));

gulp.task('js', ['js:build']);

gulp.task('default', ['js'], () => gulp.watch(js.src, ['js']));