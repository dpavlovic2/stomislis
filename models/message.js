var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MessageSchema = new Schema({
    text: String
}, {
    collection: 'Messages'
});

mongoose.model('Message', MessageSchema);