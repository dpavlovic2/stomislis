var Main = (function () {
    function createDefaultHandlers() {
        $(function () {
            var socket = io();

            socket.on('addedMessage', function(msg){
                var elem = $('#current-message');
                elem.data('carousel', false);
                elem.text(msg.message.text);
                setTimeout(function() {
                    elem.data('carousel', true);
                }, 8000);
            });

            socket.on('newMessage', function (msg) {
                var message = msg.message,
                    elem = $('#current-message');
                
                if (message.text && elem.data('carousel') === true) {
                    elem.text(message.text).data('id', message._id);
                }
            });

            socket.on('currentMessage', function (data, cb) {
                cb({
                    _id: $('#current-message').data('id')
                });
            });
        });
    }

    return {
        start: createDefaultHandlers
    };

})();

Main.start();